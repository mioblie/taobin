/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.inheritance;

/**
 *
 * @author acer
 */
public class Animal {

    private String name;
    private int NumberOfLegs = 0;
    private String color;

    public Animal(String name, String color) {
        System.out.println("Animal Created");
        this.name = name;
        this.color = color;

    }

    public void walk() {
        System.out.println("Animal Walk");
    }

    public void speak() {
        System.out.println("Animal Speak");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.overloading_method;

/**
 *
 * @author acer
 */
public class Robot {

    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection = ' ';

    public Robot(int x, int y, int bx, int by, int N) {
        this.x = x;
        this.y = y;
        this.bx = bx;
        this.by = by;
        this.N = N;
    }

    public boolean walkToDirectionOneStep(char direction) {
        return true;
    }

    public String toString() {
        return "Robot (" + this.x + "," + this.y + ")";
    }
}
